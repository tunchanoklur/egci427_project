// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import svgJs from "./vueSvgPlugin"
import Notifications from 'vue-notification'
// import firebase from 'firebase'
import firebase from 'firebase/app';
import 'firebase/app'
import 'firebase/auth';
import 'firebase/firestore';
import 'firebase/database'

Vue.config.productionTip = false

let app;
// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyDMjRG1uFYcbEdJPMnQ1JTGoHN4RQ6kBrI",
  authDomain: "egci427finalproject.firebaseapp.com",
  databaseURL: "https://egci427finalproject.firebaseio.com",
  projectId: "egci427finalproject",
  storageBucket: "egci427finalproject.appspot.com",
  messagingSenderId: "568350497520",
  appId: "1:568350497520:web:96e70ed325ad04e1"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
window.firebase = firebase;

/* eslint-disable no-new */
firebase.auth().onAuthStateChanged((user)=>{
  Vue.use(svgJs);
  Vue.use(Notifications); 
  Vue.use(Vuetify)
  if(!app){
    new Vue({
      el: '#app',
      router,
      components: { App },
      template: '<App/>'
    })
  }  
  
})



