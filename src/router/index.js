import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login'
import SignUp from '@/components/SignUp'
import ThailandMapPage from '@/components/ThailandMapPage'
import ProvincesPage from '@/components/ProvincesPage'
import EachPlacePage from '@/components/EachPlacePage'
import UserProfile from '@/components/UserProfile'


Vue.use(Router)

let router = new Router({
  routes: [
    {
      path: '/',
      redirect: '/Login'
    },
    {//type something else
      path: '*',
      redirect: '/Login'
    },
    {
      path: '/Login',
      name: 'Login',
      component: Login
    },
    {
      path: '/SignUp',
      name: 'SignUp',
      component: SignUp
    },{
      path: '/ThailandMap',
      name: 'ThailandMap',
      component: ThailandMapPage,
      meta:{
        requiresAuth:true
      }
    },
    {
      path: '/Provinces/:province',
      name: 'Provinces',
      component: ProvincesPage,
      props:true,
      meta:{
        requiresAuth:true
      }
    },
    {
      path: '/Provinces/:province/:place',
      name: 'PlaceDetail',
      component: EachPlacePage,
      props:true,
      meta:{
        requiresAuth:true
      }
    },
    {
      path: '/UserProfile',
      name: 'UserProfile',
      component: UserProfile,
      meta:{
        requiresAuth:true
      }
    }
  ]
})
//if login success = redirect to movie --> if not , go to sigup
router.beforeEach((to,from,next)=>{//authentication
  //check who already login(login status)
  let currentUser = firebase.auth().currentUser
  let requiresAuth = to.matched.some(record => record.meta.requiresAuth)
  console.log(currentUser,requiresAuth)
  if(currentUser){//if user is logged in
    console.log("currently login")
    if(requiresAuth){//if the page require authen, user can go
      console.log("page require authen")
      next()
    }else{//the page is login/sign up, user cannot go
      console.log("page no auth, can't go")
      next({path:'/ThailandMap'})
    }
  }
  else{//user did not login
    console.log("not login")
    if(to.path==="/Login" || to.path==="/SignUp"){//only allow travel to page login or sign up
      next()
    }
    else{//otherwise, bring to login page
      next('Login')
    }
  }
})
export default router;